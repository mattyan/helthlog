create table helth (
    `id` INTEGER PRIMARY KEY,
    `date` DATETIME,
    `weight` REAL,
    `bmi` REAL,
    `fat` REAL,
    `muscle` REAL,
    `bone` REAL,
    `visceral_fat` REAL,
    `metabolism` REAL,
    `age` INTEGER
);

create unique index idx_date on helth(date);
