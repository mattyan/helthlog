package main

import (
	"github.com/ipfans/echo-session"
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"

	"helth/api"
)

func main() {
	server := echo.New()
	server.Use(middleware.Recover())
	store := session.NewCookieStore([]byte("secret"))
	server.Use(session.Sessions("SESID", store))

	server.Static("/", "./")
	api.InitHelthAPI(server)

	server.Start(":8000")
}
