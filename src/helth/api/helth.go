package api

import (
	"fmt"
	"net/http"
	"time"

	"database/sql"

	"github.com/labstack/echo"
	_ "github.com/mattn/go-sqlite3"
)

// Helth 体組成データ
type Helth struct {
	Date        string  `json:"date"`
	Weight      float32 `json:"weight"`
	Bmi         float32 `json:"bmi"`
	Fat         float32 `json:"fat"`
	Muscle      float32 `json:"muscle"`
	Bone        float32 `json:"bone"`
	VisceralFat float32 `json:"visceral_fat"`
	Metabolism  float32 `json:"metabolism"`
	Age         int     `json:"age"`
}

// Result その他のレスポンス
type Result struct {
	Error   bool   `json:"error"`
	Message string `json:"message"`
}

// InitHelthAPI API初期化
func InitHelthAPI(server *echo.Echo) {
	server.GET("/api/helth/:month", GetGetHandler())
	server.POST("/api/helth", GetRegistHandler())
}

// GetGetHandler getハンドラ取得
func GetGetHandler() echo.HandlerFunc {
	return func(context echo.Context) error {
		month, err := time.Parse("2006-01", context.Param("month"))
		if err != nil {
			return context.JSON(http.StatusBadRequest, Result{Error: true, Message: "日付異常" + context.Param("month")})
		}
		lastDate := getLastDate(month.Year(), int(month.Month())).Day()
		data := []Helth{}
		for i := 1; i <= lastDate; i++ {
			helthData, err := getHelthData(time.Date(month.Year(), month.Month(), i, 0, 0, 0, 0, time.Local))
			if err != nil {
				// 取得エラー
				return context.JSON(http.StatusInternalServerError, Result{Error: true, Message: err.Error()})
			}
			data = append(data, helthData)
		}
		return context.JSON(http.StatusOK, data)
	}
}

func getLastDate(year, month int) time.Time {
	return time.Date(year, time.Month(month+1), 1, 23, 59, 59, 999999999, time.Local).AddDate(0, 0, -1)
}

func getHelthData(date time.Time) (Helth, error) {
	helth := Helth{
		Date:        fmt.Sprintf("%04d年%02d月%02d日", date.Year(), int(date.Month()), int(date.Day())),
		Weight:      0.0,
		Bmi:         0.0,
		Fat:         0.0,
		Muscle:      0.0,
		Bone:        0.0,
		VisceralFat: 0.0,
		Metabolism:  0.0,
		Age:         0,
	}

	db, err := sql.Open("sqlite3", "helth.db")
	if err != nil {
		return Helth{}, err
	}

	row := db.QueryRow("SELECT weight, bmi, fat, muscle, bone, visceral_fat, metabolism, age from helth where `date` = ?", fmt.Sprintf("%04d-%02d-%02d", date.Year(), int(date.Month()), int(date.Day())))
	var weight float32
	var bmi float32
	var fat float32
	var muscle float32
	var bone float32
	var visceralFat float32
	var metabolism float32
	var age int
	err = row.Scan(&weight, &bmi, &fat, &muscle, &bone, &visceralFat, &metabolism, &age)

	if err != nil && err != sql.ErrNoRows {
		// row無しは問題ない
		return Helth{}, err
	}
	// データコピー
	helth.Weight = weight
	helth.Bmi = bmi
	helth.Fat = fat
	helth.Muscle = muscle
	helth.Bone = bone
	helth.VisceralFat = visceralFat
	helth.Metabolism = metabolism
	helth.Age = age

	return helth, nil
}

// GetRegistHandler registハンドラ取得
func GetRegistHandler() echo.HandlerFunc {
	return func(context echo.Context) error {
		data := new(Helth)
		if err := context.Bind(data); err != nil {
			return context.JSON(http.StatusBadRequest, Result{Error: true, Message: err.Error()})
		}

		db, err := sql.Open("sqlite3", "helth.db")
		if err != nil {
			return context.JSON(http.StatusBadRequest, Result{Error: true, Message: err.Error()})
		}

		date, _ := time.Parse("2006-01-02", data.Date)
		_, err = db.Exec(
			"INSERT INTO `helth` (`date`, `weight`, `bmi`, `fat`, `muscle`, `bone`, `visceral_fat`, `metabolism`, `age`) values (?,?,?,?,?,?,?,?,?)",
			date.Format("2006-01-02"),
			data.Weight,
			data.Bmi,
			data.Fat,
			data.Muscle,
			data.Bone,
			data.VisceralFat,
			data.Metabolism,
			data.Age)

		if err != nil {
			return context.JSON(http.StatusBadRequest, Result{Error: true, Message: err.Error()})
		}

		return context.JSON(http.StatusOK, Result{Error: false, Message: "ok"})
	}
}
