var path = require('path');
var webpack = require('webpack');
module.exports = {
	'entry': {
		// login: './src/scripts/login.jsx',
		index: './js/index.jsx'
	},
	'output': {
		'filename': '[name].min.js',
		'path': path.resolve(__dirname, '../bin/assets/'),
	},
	'module': {
		'rules': [
			{
				'test': /\.jsx$/,
				'exclude': '/node_modules/',
				'loader': 'babel-loader',
				'query': {
					'presets': [
						'react',
						'env'
					]
				}
			},
			{
				'test': /\.(js|jsx)$/,
				'exclude': '/node_modules/',
				'loader': 'eslint-loader',
				'options': {
					'configFile': './.eslint',
					'fix': true,
					'failOnError': true,
				}
			}
		]
	},
	plugins: [
		new webpack.ProvidePlugin({
			$: 'jquery',
			jQuery: 'jquery',
			'window.jQuery': 'jquery',
			Popper: ['popper.js', 'default']

		})
	]
};