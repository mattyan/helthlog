import React from 'react';
import 'bootstrap';

import Dispatcher from './dispatcher.jsx';


export default class RegistModal extends React.Component {
  constructor(props) {
    super(props);

    // refs
    this.refDate = React.createRef();
    this.refWeight = React.createRef();
    this.refBMI = React.createRef();
    this.refFat = React.createRef();
    this.refMuscle = React.createRef();
    this.refBone = React.createRef();
    this.refVisceralFat = React.createRef();
    this.refMetabolism = React.createRef();
    this.refAge = React.createRef();
  }
  componentDidMount() {
    const date = new Date();
    $('#date').val(`${date.getFullYear()}-${(`0${date.getMonth() + 1}`).slice(-2)}-${(`0${date.getDate()}`).slice(-2)}`);
  }
  onClickRegist() {
    const data = {
      date: this.refDate.current.value,
      weight: parseFloat(this.refWeight.current.value),
      bmi: parseFloat(this.refBMI.current.value),
      fat: parseFloat(this.refFat.current.value),
      muscle: parseFloat(this.refMuscle.current.value),
      bone: parseFloat(this.refBone.current.value),
      visceral_fat: parseFloat(this.refVisceralFat.current.value),
      metabolism: parseFloat(this.refMetabolism.current.value),
      age: parseInt(this.refAge.current.value, 10),
    };

    Dispatcher.dispatch({ type: 'regist', data });
  }

  render() {
    return (
      <div>
        <div>
          <button className="btn" onClick={() => $('#regist-modal').modal()}>登録</button>
        </div>
        <div className="modal" id="regist-modal" role="dialog">
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="modal-header">
                <h4 className="modal-title">登録</h4>
                <button type="button" className="close" data-dismiss="modal"><span>&#215;</span></button>

              </div>
              <div className="modal-body">
                <form>
                  <div className="form-group"><label htmlFor="date">日付<input type="date" id="date" name="date" ref={this.refDate} className="form-control" /></label></div>
                  <div className="form-group"><label htmlFor="weight">体重(kg)<input type="number" name="weight" ref={this.refWeight} className="form-control" min="0" step="0.05" /></label></div>
                  <div className="form-group"><label htmlFor="bmi">BMI<input type="number" name="bmi" ref={this.refBMI} className="form-control" min="0" step="0.1" /></label></div>
                  <div className="form-group"><label htmlFor="fat">体脂肪率(%)<input type="number" name="fat" ref={this.refFat} className="form-control" min="0" step="0.1" /></label></div>
                  <div className="form-group"><label htmlFor="muscle">筋肉量(kg)<input type="number" name="muscle" ref={this.refMuscle} className="form-control" min="0" step="0.01" /></label></div>
                  <div className="form-group"><label htmlFor="bone">骨量(kg)<input type="number" name="bone" ref={this.refBone} className="form-control" min="0" step="0.1" /></label></div>
                  <div className="form-group"><label htmlFor="visceral_fat">内臓脂肪レベル<input type="number" name="visceral_fat" ref={this.refVisceralFat} className="form-control" min="0" step="0.1" /></label></div>
                  <div className="form-group"><label htmlFor="metabolism">基礎代謝(kcal/日)<input type="number" name="metabolism" ref={this.refMetabolism} className="form-control" min="0" step="0.1" /></label></div>
                  <div className="form-group"><label htmlFor="age">体内年齢(歳)<input type="number" name="age" ref={this.refAge} className="form-control" min="0" /></label></div>
                </form>
              </div>
              <div className="modal-footer">
                <button type="button" className="btn btn-danger" data-dismiss="modal">キャンセル</button>
                <button type="button" className="btn btn-primary" data-dismiss="modal" onClick={() => this.onClickRegist()}>登録</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
