import React from 'react';
import Dispatcher from './dispatcher.jsx';
/**
 * 月選択
 */
export default class MonthSelector extends React.Component {
  /**
   * コンストラクタ
   * @param {*} props
   */
  constructor(props) {
    super(props);
    this.state = {
      month: new Date(),
    };
  }
  /**
   * 先月に移動
   */
  onClickLastMonth() {
    const date = this.state.month;
    date.setMonth(this.state.month.getMonth() - 1);
    this.setState({
      month: date,
    });
    const month = (`0${(this.state.month.getMonth() + 1)}`).substr(-2);
    Dispatcher.dispatch({ type: 'get', month: `${this.state.month.getFullYear()}-${month}` });
  }

  /**
   * 来月に移動
   */
  onClickNextMonth() {
    const date = this.state.month;
    date.setMonth(this.state.month.getMonth() + 1);
    this.setState({
      month: date,
    });
    const month = (`0${(this.state.month.getMonth() + 1)}`).substr(-2);
    Dispatcher.dispatch({ type: 'get', month: `${this.state.month.getFullYear()}-${month}` });
  }


  render() {
    return (
      <div id="month-select">
        <button className="btn" onClick={() => { this.onClickLastMonth(); return false; }}>先月</button>
        <span className="h1" id="month">{this.state.month.getFullYear()}年{this.state.month.getMonth() + 1}月</span>
        <button className="btn" onClick={() => { this.onClickNextMonth(); return false; }}>来月</button>
      </div>
    );
  }
}
