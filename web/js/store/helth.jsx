import { EventEmitter } from 'events';
import Dispatcher from '../dispatcher.jsx';

class HelthStore extends EventEmitter {
  constructor() {
    super();
    this.data = [];
    this.month = null;
  }

  /**
     * イベントハンドラ
     * @param {*} action
     */
  handle(action) {
    if (action.type === 'get') {
      this.get(action.month);
    } else if (action.type === 'update') {
      this.get(null);
    } else if (action.type === 'regist') {
      this.regist(action.data);
    }
  }

  /**
     * 体組成データ取得
     * @param {*} month
     */
  get(month) {
    if (month !== null) {
      this.month = month;
    }
    fetch(`/api/helth/${this.month}`).then(response => response.json()).then((data) => {
      if (data.error) {
        console.log(data.message);
        return;
      }
      this.data = data;
      this.emit('update');
    });
  }

  /**
     * 体組成データ登録
     * @param {*} action
     */
  regist(data) {
    fetch('/api/helth', {
      method: 'POST',
      body: JSON.stringify(data),
      headers: new Headers({ 'Content-Type': 'application/json' }),
    }).then(() => {
      // 更新が終わったら再取得
      this.get(null);
    });
  }
}
// イベント登録
const store = new HelthStore();
Dispatcher.register(store.handle.bind(store));
export default store;
