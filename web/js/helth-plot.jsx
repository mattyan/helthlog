import React from 'react';
import 'updated-jqplot';
import 'updated-jqplot/dist/plugins/jqplot.dateAxisRenderer';
import Store from './store/helth.jsx';

export default class HelthPlot extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      empty: [],
      weight: [],
      is_weight: false,
      bmi: [],
      is_bmi: false,
      fat: [],
      is_fat: false,
      muscle: [],
      is_muscle: false,
      bone: [],
      is_bone: false,
      visceral_fat: [],
      is_visceral_fat: false,
      metabolism: [],
      is_metabolism: false,
      age: [],
      is_age: false,
      start_date: '',
      end_date: '',
    };
  }
  componentWillMount() {
    Store.on('update', () => {
      this.state.start_date = Store.data[0].date.replace(/(\d+)年(\d+)月(\d+)日/, '$1-$2-$3');
      // 初期化
      this.state.empty = [];
      this.state.weight = [];
      this.state.bmi = [];
      this.state.fat = [];
      this.state.muscle = [];
      this.state.bone = [];
      this.state.visceral_fat = [];
      this.state.metabolism = [];
      this.state.age = [];

      Store.data.forEach((data) => {
        const date = data.date.replace(/(\d+)年(\d+)月(\d+)日/, '$1-$2-$3');
        this.state.empty.push([date, 0]);
        this.state.weight.push([date, data.weight]);
        this.state.bmi.push([date, data.bmi]);
        this.state.fat.push([date, data.fat]);
        this.state.muscle.push([date, data.muscle]);
        this.state.bone.push([date, data.bone]);
        this.state.visceral_fat.push([date, data.visceral_fat]);
        this.state.metabolism.push([date, data.metabolism]);
        this.state.age.push([date, data.age]);
        this.state.end_date = date;
      });
      this.setState(this.state);
    });
  }
  componentDidUpdate() {
    $('#plot-area').empty();
    const data = [];
    // プロットデータ決定
    if (this.state.is_weight) {
      // 体重
      data.push(this.state.weight);
    }
    if (this.state.is_bmi) {
      // BMI
      data.push(this.state.bmi);
    }
    if (this.state.is_fat) {
      // 体脂肪率
      data.push(this.state.fat);
    }
    if (this.state.is_muscle) {
      // 筋肉量
      data.push(this.state.muscle);
    }
    if (this.state.is_bone) {
      // 骨量
      data.push(this.state.bone);
    }
    if (this.state.is_visceral_fat) {
      // 内臓脂肪レベル
      data.push(this.state.visceral_fat);
    }
    if (this.state.is_metabolism) {
      // 基礎代謝
      data.push(this.state.metabolism);
    }
    if (this.state.is_age) {
      // 体内年齢
      data.push(this.state.age);
    }

    if (data.length === 0) {
      data.push(this.state.empty);
    }
    $('#plot-area').jqplot(data, {
      axes: {
        xaxis: {
          renderer: $.jqplot.DateAxisRenderer,
          min: this.state.start_date,
          max: this.state.end_date,
          tickInterval: '1 days',
          tickOptions: { formatString: '%d' },
        },
      },
    });
  }
  onClickFilter(event) {
    const key = event.currentTarget.name;
    this.setState({
      [key]: !this.state[key],
    });
  }

  render() {
    return (
      <div>
        <div id="plot-filter">
          <form action="#" method="GET">
            <label htmlFor="is_weight" className="checkbox-inline"><input type="checkbox" name="is_weight" onChange={event => this.onClickFilter(event)} />体重</label>
            <label htmlFor="is_bmi" className="checkbox-inline"><input type="checkbox" name="is_bmi" onChange={event => this.onClickFilter(event)} />BMI</label>
            <label htmlFor="is_fat" className="checkbox-inline"><input type="checkbox" name="is_fat" onChange={event => this.onClickFilter(event)} />体脂肪率</label>
            <label htmlFor="is_muscle" className="checkbox-inline"><input type="checkbox" name="is_muscle" onChange={event => this.onClickFilter(event)} />筋肉量</label>
            <label htmlFor="is_bone" className="checkbox-inline"><input type="checkbox" name="is_bone" onChange={event => this.onClickFilter(event)} />骨量</label>
            <label htmlFor="is_visceral_fat" className="checkbox-inline"><input type="checkbox" name="is_visceral_fat" onChange={event => this.onClickFilter(event)} />内臓脂肪レベル</label>
            <label htmlFor="is_metabolism" className="checkbox-inline"><input type="checkbox" name="is_metabolism" onChange={event => this.onClickFilter(event)} />基礎代謝</label>
            <label htmlFor="is_age" className="checkbox-inline"><input type="checkbox" name="is_age" onChange={event => this.onClickFilter(event)} />体内年齢</label>
          </form>
        </div>
        <div id="plot-area" />
      </div>
    );
  }
}
