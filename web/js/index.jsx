import React from 'react';
import ReactDOM from 'react-dom';

import MonthSelector from './month-selector.jsx';
import HelthTable from './helth-table.jsx';
import HelthPlot from './helth-plot.jsx';
import Dispatcher from './dispatcher.jsx';
import RegistModal from './regist-modal.jsx';


class IndexComponent extends React.Component {
  componentDidMount() {
    const date = new Date();
    const year = date.getFullYear();
    const month = (`0${date.getMonth() + 1}`).substr(-2);
    Dispatcher.dispatch({ type: 'get', month: `${year}-${month}` });
  }
  render() {
    return (
      <div className="container">
        <MonthSelector />
        <HelthPlot />
        <RegistModal />
        <HelthTable />
      </div>
    );
  }
}

ReactDOM.render(<IndexComponent />, document.getElementById('contents'));
