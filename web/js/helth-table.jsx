import React from 'react';
import Store from './store/helth.jsx';

export default class HelthTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: [] };
  }

  componentWillMount() {
    Store.on('update', () => {
      this.setState({ data: Store.data });
    });
  }

  render() {
    return (
      <div id="data">
        <table className="table">
          <thead>
            <tr>
              <th>日付</th>
              <th>体重(kg)</th>
              <th>BMI</th>
              <th>体脂肪率(%)</th>
              <th>筋肉量(kg)</th>
              <th>骨量(kg)</th>
              <th>内臓脂肪レベル</th>
              <th>基礎代謝(kcal/日)</th>
              <th>体内年齢(歳)</th>
            </tr>
          </thead>
          <tbody>
            {this.state.data.map(data => (
              <tr>
                <td>{data.date}</td>
                <td>{data.weight}</td>
                <td>{data.bmi}</td>
                <td>{data.fat}</td>
                <td>{data.muscle}</td>
                <td>{data.bone}</td>
                <td>{data.visceral_fat}</td>
                <td>{data.metabolism }</td>
                <td>{data.age}</td>
              </tr>
                ))}
          </tbody>
        </table>
      </div>
    );
  }
}
