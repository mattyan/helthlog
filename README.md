# 概要
タニタの体組成計の測定結果を入力するとグラフにしてくれるツール

# install
```sh
git clone
cd helthlog

# ライブラリの取得
cd web
npm install
cd ../
cd src/helth
dep ensure
cd ../

# ビルド
go install -v -gcflags '-N -l' ./...
cd ../../web
npm run make
```

# db用意
binディレクトリにて
```sh
sqlite3 helth.db
```

で以下のsqlを実行
```sql
create table helth (
    `id` INTEGER PRIMARY KEY,
    `date` DATETIME,
    `weight` REAL,
    `bmi` REAL,
    `fat` REAL,
    `muscle` REAL,
    `bone` REAL,
    `visceral_fat` REAL,
    `metabolism` REAL,
    `age` INTEGER
);
create unique index idx_date on helth(date);

```

# 実行
```sh
cd bin
./helth
```
その後サーバーのIPアドレス:8000に接続
